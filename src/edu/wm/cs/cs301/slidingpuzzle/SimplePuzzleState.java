package edu.wm.cs.cs301.slidingpuzzle;

import java.util.Arrays;
import java.util.LinkedList;

import edu.wm.cs.cs301.slidingpuzzle.PuzzleState.Operation;

public class SimplePuzzleState implements PuzzleState {

	//slots is a two dimensional array for storing value
	private int[][] slots;
	//here we store the last move
	private Operation operation;
	//here we store last state
	private PuzzleState prev;
	//here we store how far the puzzle has changed away from the start
	private int length;
	
	//constructor
	public SimplePuzzleState() {
		super();
		this.slots = null;
		this.operation = null;
		this.prev = null;
		this.length = 0;
	}
	//Store objects here
	public SimplePuzzleState(int[][] valueTable, Operation operation, PuzzleState parent, int pathLength) {
		this.slots = valueTable;
		this.operation = operation;
		this.prev = parent;
		this.length = pathLength;
	}	
	
	@Override
	public void setToInitialState(int dimension, int numberOfEmptySlots) {
		// TODO Auto-generated method stub
		
		//fill the table with the dimensions given
		this.slots = new int[dimension][dimension];
		
		int count = 1;
		for (int i=0; i < dimension; i++) {
			for (int j=0; j < dimension; j++) {
				this.slots[i][j] = count;
				count++;
			}
		}
		if (numberOfEmptySlots == 1) {
			slots[dimension-1][dimension-1] = 0;
		}
		if (numberOfEmptySlots == 2) {
			slots[dimension-1][dimension-1] = 0;
			slots[dimension-1][dimension-2] = 0;
		}
		if (numberOfEmptySlots == 3) {
			slots[dimension-1][dimension-1] = 0;
			slots[dimension-1][dimension-2] = 0;
			slots[dimension-1][dimension-3] = 0;
		}
	}

	@Override
	public int getValue(int row, int column) {
		// TODO Auto-generated method stub
		//if it is at all possible to retrieve a value with the given row and column... 
		if (row >= 0 && column >= 0 && row < slots.length && column < slots.length){
			//return the value at the row and column
			return slots[row][column];
		}
		//just return -1 in the event we get bad news
		return -1;
	}

	@Override
	public PuzzleState getParent() {
		// TODO Auto-generated method stub
		//just return current parent
		return this.prev;
	}

	//auto-generated
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(slots);
		return result;
	}

	//auto-generated
	@Override
	public boolean equals(Object obj) {
		if (obj == null || obj.getClass() != getClass())
			return false;
		
		if(obj == this)
			return true;
		
		SimplePuzzleState other = (SimplePuzzleState) obj;
		
		if(Arrays.deepEquals(other.slots, slots))
			return true;
		return false;
	}

	@Override
	public Operation getOperation() {
		// TODO Auto-generated method stub
		
		//return this.operation
		return this.operation;
	}

	@Override
	public int getPathLength() {
		// TODO Auto-generated method stub
		//return the path length 
		return this.length;
	}

	@Override
	public PuzzleState move(int row, int column, Operation op) {
		// TODO Auto-generated method stub
		
		//if space clicked is free, don't move
		if (getValue(row, column) == 0) {
			return null;
		}
		
		//spawn a new table to mess with
		PuzzleState other = new SimplePuzzleState();
		((SimplePuzzleState)other).slots = new int[slots.length][slots.length];
		//for every row
		for (int r = 0; r < slots.length; r++) {
			//for every column
			for (int j = 0; j < slots[0].length; j++) {
				//copy the values
				((SimplePuzzleState)other).slots[r][j] = this.getValue(r, j);
			}
		}
		
		//check if moving right is possible first
		switch (op) {
		//try right move
		case MOVERIGHT:
			//if the move is not out of bounds ...
			if (column + 1 < slots.length) {
				//if the space is free...
				if (isEmpty(row, column + 1)) {
					int temp= ((SimplePuzzleState)other).slots[row][column];
					((SimplePuzzleState)other).prev = this;
					((SimplePuzzleState)other).slots[row][column] = 0;
					((SimplePuzzleState)other).slots[row][column + 1] = temp;
					((SimplePuzzleState)other).length = this.getPathLength() + 1;
					((SimplePuzzleState)other).operation = op;
					}else {
						return null;
					}
				}else {
						return null;
					}
				break;
		//so on and so forth with left, down, and up
		case MOVELEFT:
			if (column - 1 >= 0) {
				if (isEmpty(row, column - 1)) {
					int temp = ((SimplePuzzleState)other).slots[row][column];
					((SimplePuzzleState)other).prev = this;
					((SimplePuzzleState)other).slots[row][column] = 0;
					((SimplePuzzleState)other).slots[row][column - 1] = temp;
					((SimplePuzzleState)other).length = this.getPathLength() + 1;
					((SimplePuzzleState)other).operation = op;
				} else {
					return null;
				}
			} else {
				return null;
			}
			break;
		case MOVEUP:
			if (row - 1 >= 0){
				if (isEmpty(row - 1, column)){
					int temp = ((SimplePuzzleState)other).slots[row][column];					
					((SimplePuzzleState)other).prev = this;
					((SimplePuzzleState)other).slots[row][column] = 0;
					((SimplePuzzleState)other).slots[row - 1][column] = temp;
					((SimplePuzzleState)other).length = this.getPathLength() + 1;
					((SimplePuzzleState)other).operation = op;
				} else {
					return null;
				}
			} else {
				return null;
			}
			break;
		case MOVEDOWN:
			if (row + 1 < slots.length){
				if (isEmpty(row + 1, column)){
					int temp = ((SimplePuzzleState)other).slots[row][column];
					((SimplePuzzleState)other).prev = this;
					((SimplePuzzleState)other).slots[row][column] = 0;
					((SimplePuzzleState)other).slots[row+1][column] = temp;
					((SimplePuzzleState)other).length = this.getPathLength() + 1;
					((SimplePuzzleState)other).operation = op;
				}
				else {
					return null;
				}
			}
			else {
				return null;
			}
			break;
	
		default:
			break;
		}
		
		return other;
	}

	@Override
	public PuzzleState drag(int startRow, int startColumn, int endRow, int endColumn) {
		// TODO Auto-generated method stub
		
		//this time lets just spawn another state with the same stuff as our current state, for a change
		PuzzleState next = new SimplePuzzleState();
		//same stuff as current state, please
		next = this;
		
		//need to know how far away in rows and columns we are from where we want to go
		int rowdifference = (endRow + 1) - (startRow + 1);
		int columndifference = (endColumn + 1) - (startColumn + 1);
		
		PuzzleState other = new SimplePuzzleState();
		((SimplePuzzleState)other).slots = this.slots.clone();
		((SimplePuzzleState)other).operation = this.getOperation();
		((SimplePuzzleState)other).prev = this.getParent();		
		((SimplePuzzleState)other).length = this.getPathLength();

		
		while (rowdifference != 0 || columndifference != 0){
			if (columndifference < 0 && isEmpty(startRow, startColumn - 1)) {
				other =  other.move(startRow, startColumn, Operation.MOVELEFT);
				startColumn--;
				columndifference++;
				
			}			
			if (columndifference > 0 &&isEmpty(startRow, startColumn + 1)) {
				other = other.move(startRow, startColumn, Operation.MOVERIGHT);
				
				
				startColumn++;
				columndifference--;
				
			}
			if (isEmpty(startRow - 1, startColumn) && rowdifference < 0) {
				other =  other.move(startRow, startColumn, Operation.MOVEUP);
				startRow--;
				rowdifference++;
			}			
			if (isEmpty(startRow + 1, startColumn) && rowdifference > 0) {
				other = other.move(startRow, startColumn, Operation.MOVEDOWN);
				startRow++;
				rowdifference--;
				
			}
		}
		return other;
	}


	@Override
	public PuzzleState shuffleBoard(int pathLength) {
		
		//same thing that we did in drag
		PuzzleState next = new SimplePuzzleState();
		next = this;
		
		//while path length is greater than 0
		while (pathLength > 0) {
			//while we're not broken...
			while (true) {
				//use linked lists to collect and store where all the free spaces are
				LinkedList<Integer> freeColumns = new LinkedList<Integer>();				
				LinkedList<Integer> freeRows = new LinkedList<Integer>();
				for (int r = 0; r < slots.length; r++) {
					//for every column and row...
					for (int j = 0; j < slots.length; j++) {
						if (isEmpty(r,j)) {
							freeColumns.add(j);							
							freeRows.add(r);

						}
					}
				}
				//pick a number from the amount of free spaces i have to determine which free space im going to manipulate
				int randomemptyspace = (int) Math.floor(Math.random() * freeRows.size());
				//get the random Row now
				int rowstarter = freeRows.get(randomemptyspace);
				//along with the matching random Column
				int columnstarter = freeColumns.get(randomemptyspace);
				//get me a random direction to move in
				int random = (int) Math.floor(Math.random() * 4);
				//until further notice, a new operation is null, to be set at a later date
				Operation newop = null;
				//consideration for each possible move
				if (random == 0) {
					newop = Operation.MOVEUP;
					rowstarter++;
				} else if (random == 1) {
					newop = Operation.MOVEDOWN;
					rowstarter--;
				} else if (random == 2) {
					newop = Operation.MOVELEFT;
					columnstarter++;
				} else if (random == 3) {
					newop = Operation.MOVERIGHT;
					columnstarter--;
				}
				
				//all this below ensures we do not randomly go back where we once were
				if (this.getOperation() == Operation.MOVEUP && newop == Operation.MOVEDOWN) {
					//dont let it move up then
					continue;
				} else if (this.getOperation() == Operation.MOVEDOWN && newop == Operation.MOVEUP) {
					continue;
				} else if (this.getOperation() == Operation.MOVELEFT && newop == Operation.MOVERIGHT) {
					continue;
				} else if (this.getOperation() == Operation.MOVERIGHT && newop == Operation.MOVELEFT) {
					continue;
				}
				
				//if what we're about to mess with is not in range
				if (rowstarter < 0 || rowstarter >= slots.length || columnstarter < 0 || columnstarter >= slots.length) {
					//send the while loop back again, get a different value
					continue;
				}
				//if what we're about to mess with is another fellow empty space
				if (isEmpty(rowstarter, columnstarter)) {
					//send it back again ....
					continue;
				}
				//we're assuming we've caught everything, and that what we're about to mess with is good
				PuzzleState result = this.move(rowstarter, columnstarter, newop);
				next = result;
				//break out of the while loop
				break;
			}
			//recursively call ourselves again, decrementing by 1
			return next.shuffleBoard(pathLength - 1);
		}
		//we return ourselves since we trust that the aforementioned recursion has brought us to where we'd like to be 
		return this;
	}

	@Override
	public boolean isEmpty(int row, int column) {
		// TODO Auto-generated method stub
		//make sure the block being asked for is NOT inexistent
		if (row >= 0 && column >= 0 && row < slots.length && column < slots.length){	
			//check if its free or not
			if (this.slots[row][column] == 0) {
				//say its free
				return true;
			}
		}
		//otherwise, its not
		return false;
	}

	@Override
	public PuzzleState getStateWithShortestPath() {
		// TODO Auto-generated method stub
		//honestly took a stab that this might be it, and it was
		return this;
	}
}